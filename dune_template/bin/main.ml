(* open Base *)
open Ocamlgemini

let homepage = "gemini://gemini.circumlunar.space/"

let main =
  Lwt.join [
    Demo.print homepage;
(*  
    let open Lwt in
    let uri = Uri.of_string homepage  in
    Network.get_io_channels uri "1965" >>= (* TCP connexion *)
      Network.get_and_print_raw uri (* URI request and print *) 
*)
  ]

let () = Lwt_main.run main

